﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Chomp_.Characters
{

    

    public class Velocity
    {
        public float minimum { set; get; }
        public float maximum { set; get; }
        public float yVelocity { set; get; }

        public Velocity(float min, float max, float yVel)
        {
            minimum = min;
            maximum = max;
            yVelocity = yVel;
        }
    }

    public class Character
    {

        public string name;
        public Game1 game;
        
        public Sprite sprite;

        public Boolean ground;

        public Velocity vel = new Velocity(-10f, 25f, 0.2f);
        public float speed;

        public Boolean showName;

        public Boolean alive;

        public Character(Game1 game, string texturePath, Point initPos)
        {
            this.name = "Character";

            this.game = game;
            this.sprite = new Sprite(game, texturePath, initPos, new Vector2(1, 1), false);

            this.speed = 1;
            this.showName = false;

            this.alive = true;
        }

        public Character(Game1 game, string texturePath, Point initPos, Point spriteDim)
        {
            this.name = "Character";
            
            this.game = game;
            this.sprite = new Sprite(game, texturePath, initPos, spriteDim, new Vector2(1, 1), false);

            this.speed = 1;
            this.showName = false;

            this.alive = true;
        }

        public Character(Game1 game, string texturePath, Point initPos, Point spriteDim, float speed)
        {
            this.name = "Character";

            this.game = game;
            this.sprite = new Sprite(game, texturePath, initPos, spriteDim, new Vector2(1, 1), false);

            this.speed = speed;
            this.showName = false;

            this.alive = true;
        }


        public virtual void Update(GameTime gameTime)
        {
            

            if(vel.yVelocity > vel.maximum)
                vel.yVelocity = vel.maximum;

            if (vel.yVelocity < vel.minimum)
                vel.yVelocity = vel.minimum;

            if (!game.paused)
            {
                if (!ground)
                {
                    vel.yVelocity += 0.2f;
                    Translate(0, (int) vel.yVelocity);
                }

                // If we go below the game screen, reset position to bottom of screen and remove velocity
                if (sprite.spriteArea.Y >= game.Window.ClientBounds.Height - sprite.spriteArea.Height)
                {
                    ground = true;
                    sprite.spriteArea = new Rectangle(sprite.spriteArea.X, game.Window.ClientBounds.Height - sprite.spriteArea.Height, sprite.spriteArea.Width, sprite.spriteArea.Height);
                    vel.yVelocity = 0f;
                    Translate(0, (int) vel.yVelocity);

                }
            }

        }

        public virtual void Translate(float x, float y)
        {
            this.sprite.spriteArea = new Rectangle(
                this.sprite.spriteArea.X + (int) x,
                this.sprite.spriteArea.Y + (int)y,
                this.sprite.spriteArea.Width,
                this.sprite.spriteArea.Height);
        }

        public virtual void Draw(GameTime gameTime)
        {

            SpriteFont NameFont = game.Content.Load<SpriteFont>("Fonts/Arial");

            string output = name;

            output += " (" + sprite.spriteArea.X + ", " + sprite.spriteArea.Y + ")";

            Vector2 outputArea = NameFont.MeasureString(output);
            if (this.alive)
            {
                if (this.showName)
                    game.spriteBatch.DrawString(
                        NameFont,
                        output,
                        new Vector2(sprite.spriteArea.X - (outputArea.X / 4),
                                    sprite.spriteArea.Y - outputArea.Y - 8),
                        Color.Black,
                        0,
                        new Vector2(0, 0),
                        1f,
                        SpriteEffects.None,
                        1
                    );

                sprite.Draw(gameTime);
            }
        }

        public static Vector2 MoveTowards(Vector2 position, Vector2 target, float speed)
        {
            double direction = (float)(Math.Atan2(target.Y - position.Y, target.X - position.X) * 180 / Math.PI);

            Vector2 move = new Vector2(0, 0);

            move.X = (float)Math.Cos(direction * Math.PI / 180) * speed;
            move.Y = (float)Math.Sin(direction * Math.PI / 180) * speed;

            return move;
        }
    }
}
