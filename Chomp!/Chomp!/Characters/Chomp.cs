﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Chomp_.Characters
{
    public class Chomp : Sprite
    {

        private Random random = new Random();
        private Vector2[] targets;
        public int pos_last;
        public int pos_next;

        private float lastFoodUpdate;
        private float foodDelay;

        public int desiredFood;

        public int wrong_foods_eaten;

        public Vector2 DistanceToTravel;
        private float speed;
        public Point FramesNeeded;

        public Chomp(Game1 game, String texture, Point position)
            : base(game, texture, position, new Point(125, 115), new Vector2(4, 4), true)
        {
            desiredFood = 0;
            targets = new Vector2[10];
            for (int i = 0; i < targets.Length; i++)
            {
                targets[i] = new Vector2(random.Next(game.Window.ClientBounds.Width / 2, game.Window.ClientBounds.Width - spriteArea.Width), random.Next(0, game.Window.ClientBounds.Height - spriteArea.Height));
            }

            foodDelay = 1000 * 5f;
            lastFoodUpdate = 0f;

            wrong_foods_eaten = 0;

            pos_last = 0;
            pos_next = 1;

            DistanceToTravel = Vector2.Zero;
            speed = 5f;

        }

        public Vector2 GetNextTarget()
        {
            return targets[pos_next];
        }

        public void SetMovingToNextTarget()
        {
            pos_last = pos_next;
            pos_next++;

            if (pos_next > targets.Length - 1)
                pos_next = 0;
        }

        public void SwitchDesiredFood(){
            desiredFood++;
            if (desiredFood > game.launcher.AmmoTypes.Length - 1)
                desiredFood = 0;
        }

        private Vector2 CalculatePositionVector()
        {
            return new Vector2(this.spriteArea.X, this.spriteArea.Y);
        }

        public override void Update(GameTime time)
        {

            if (Math.Abs(CalculatePositionVector().X - GetNextTarget().X) > 3 && Math.Abs(CalculatePositionVector().Y - GetNextTarget().Y) > 3)
            {
                Vector2 amountToMove = Character.MoveTowards(CalculatePositionVector(), GetNextTarget(), speed);
                this.spriteArea.X += (int)amountToMove.X;
                this.spriteArea.Y += (int)amountToMove.Y;
            }
            else
            {
                SetMovingToNextTarget();
            }

            lastFoodUpdate += (int)time.ElapsedGameTime.TotalMilliseconds;
            if (lastFoodUpdate >= foodDelay)
            {
                SwitchDesiredFood();
                lastFoodUpdate = 0;
                foodDelay = random.Next(1, 5) * 1000f; // Sets food delay to anywhere between 1 and 5 seconds
            }

            base.Update(time);
        }

        public override void Draw(GameTime gameTime)
        {

            Sprite desiredFoodSprite = new Sprite(game, @"Sprite/Foods/" + game.launcher.AmmoTypes[desiredFood], new Point(game.chomper.spriteArea.X, game.chomper.spriteArea.Y), new Vector2(1, 1), false);

            desiredFoodSprite.spriteArea.Y = desiredFoodSprite.spriteArea.Y - desiredFoodSprite.spritesheet.Bounds.Height - 4;

            desiredFoodSprite.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
