﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Chomp_.Projectile
{
    public class Projectile : Sprite
    {

        int foodType;

        public Projectile(Game1 game, String texturePath, Point initPos, int foodType) : base(game, texturePath, initPos, new Vector2(1, 1), false) {
            this.foodType = foodType;
        }

        public override void Update(GameTime gameTime)
        {

            if (alive)
            {

                if (this.spriteArea.X < game.Window.ClientBounds.Width)
                {
                    this.spriteArea.X+=10;
                }
                else
                {
                    this.alive = false;
                    game.launcher.health -= 10;
                    game.launcher.misses++;
                }

                Rectangle collision = new Rectangle(this.game.chomper.spriteArea.X, this.game.chomper.spriteArea.Y, this.game.chomper.spriteArea.Width, this.game.chomper.spriteArea.Height / 2);

                if (collision.Intersects(this.spriteArea))
                {
                    if (this.foodType == this.game.chomper.desiredFood)
                    {
                        alive = false;
                        game.launcher.hits++;
                    }
                    else
                    {
                        game.chomper.wrong_foods_eaten++;
                        alive = false;
                    }
                }
            }
            

            base.Update(gameTime);
        }

        public static Projectile New(Launcher launcher, int type)
        {
            return new Projectile(launcher.game, @"Sprite/Foods/" + launcher.AmmoTypes[type], new Point(launcher.texture.spriteArea.X, launcher.texture.spriteArea.Y), type);

        }
    }
}
