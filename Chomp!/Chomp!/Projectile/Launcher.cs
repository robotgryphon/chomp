using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Chomp_.Projectile
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Launcher : Microsoft.Xna.Framework.GameComponent
    {

        public Game1 game;
        public Sprite currentAmmo;
        public String[] AmmoTypes;
        public int CurrentAmmoType;

        public Sprite texture;

        public Boolean alive;

        public int misses;
        public int hits;

        public int health;

        private int angleX;
        private int angleY;

        public ProjectileHandler projectiles;

        public Launcher(Game1 game) : base(game)
        {

            this.game = game;

            this.texture = new Sprite(game, @"Sprite/Launcher", new Point(0, 50), new Vector2(1, 1), false);

            projectiles = new ProjectileHandler(this, 10, 60f);

            AmmoTypes = new String[] {"donut", "fries", "cake", "pretzel"};
            CurrentAmmoType = -1;

            health = 100;
            misses = 0;
            hits = 0;

            angleX = 10;
            angleY = 1;

            SwitchAmmo();
        }

        public Point getAngles()
        {
            return new Point(angleX, angleY);
        }

        public void Move(String direction)
        {
            if (direction == "up")
                this.texture.spriteArea.Y-=5;

            if (direction == "down")
                this.texture.spriteArea.Y+=5;

            if (direction == "forward")
                angleX++;

            if (direction == "back")
                angleX--;
        }

        public void SwitchAmmo()
        {
            CurrentAmmoType++;
            if (CurrentAmmoType > AmmoTypes.Length - 1)
                CurrentAmmoType = 0;

            currentAmmo = new Sprite(
                game,
                @"Sprite/Foods/" + AmmoTypes[CurrentAmmoType],
                new Point(4, 4),
                new Vector2(1, 1),
                false);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {

            if (health <= 0)
                alive = false;

            base.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            this.texture.Draw(gameTime);
        }
    }
}
