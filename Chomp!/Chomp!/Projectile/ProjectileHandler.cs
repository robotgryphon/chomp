﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Chomp_
{
    public class ProjectileHandler
    {

        private Projectile.Launcher launcher;
        private Projectile.Projectile[] projectiles;
        public int max_size;
        public int number_onscreen;
        private float timeSinceLastSpawn;
        public float delay;

        public ProjectileHandler(Projectile.Launcher launcher, int size, float delay)
        {
            this.launcher = launcher;
            max_size = size;
            projectiles = new Projectile.Projectile[size];
            number_onscreen = 0;
            timeSinceLastSpawn = 0;
            this.delay = delay;
        }

        public Projectile.Projectile GetProjectile(int pos)
        {
            return projectiles[pos];
        }

        public void RemProjectile(int pos)
        {
            projectiles[pos] = null;
            number_onscreen--;
        }

        public void AddProjectile(int x, int y, int xSpeed, int ySpeed)
        {
            for (int i = 0; i < max_size; i++)
            {
                if (projectiles[i] == null)
                {
                    projectiles[i] = Projectile.Projectile.New(launcher, launcher.CurrentAmmoType);
                    number_onscreen++;

                    break;
                }
            }
        }

        public void Clear()
        {
            for (int i = 0; i < projectiles.Length; i++ )
            {
                RemProjectile(i);
            }
        }

        public void Update(Game1 game, GameTime time)
        {

            for (int c = 0; c < max_size; c++)
            {
                if (GetProjectile(c) != null)
                {
                    if (GetProjectile(c).alive)
                    {
                            GetProjectile(c).Update(time);
                    }
                    else
                    {
                        RemProjectile(c);
                    }
                }
            }

            timeSinceLastSpawn += time.ElapsedGameTime.Milliseconds;
        }
    }
}
