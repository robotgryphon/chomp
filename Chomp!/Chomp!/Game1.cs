﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Chomp_
{

    public class InputState
    {
        public GamePadState[] controllers = new GamePadState[4];
        public KeyboardState keyboard;

        public void Update()
        {
            controllers[0] = GamePad.GetState(PlayerIndex.One);
            controllers[1] = GamePad.GetState(PlayerIndex.Two);
            controllers[2] = GamePad.GetState(PlayerIndex.Three);
            controllers[3] = GamePad.GetState(PlayerIndex.Four);

            keyboard = Keyboard.GetState();
        }

    }

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public Boolean paused = false;

        InputState oldIS;
        InputState newIS;

        SpriteFont quartz;
        SpriteFont quartz_sm;
        SpriteFont arial;

        public Projectile.Launcher launcher;
        public Characters.Chomp chomper;

        int targetSeconds;
        public int remainingSeconds;
        int thisGameStartSeconds;

        private Boolean gameover = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            oldIS = new InputState();
            newIS = new InputState();

            oldIS.Update();
            newIS.Update();

            launcher = new Projectile.Launcher(this);

            targetSeconds = 60 * 5;
            remainingSeconds = targetSeconds;
            thisGameStartSeconds = 0;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            quartz = Content.Load<SpriteFont>("Fonts/Quartz");
            quartz_sm = Content.Load<SpriteFont>("Fonts/Quartz-Small");
            arial = Content.Load<SpriteFont>("Fonts/Arial");

            chomper = new Characters.Chomp(this, @"Sprite/Chomper", new Point(650, 370));

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            newIS.Update();

            if (launcher.health > 0 & remainingSeconds > 0 & launcher.misses < 5 & chomper.wrong_foods_eaten < 3)
            {
                
                launcher.projectiles.Update(this, gameTime);

                launcher.Update(gameTime);
                chomper.Update(gameTime);

                remainingSeconds = targetSeconds - ((int) gameTime.TotalGameTime.TotalSeconds - thisGameStartSeconds);
            }
            else
            {
                gameover = true;
            }

            UpdateInput(gameTime);

            oldIS.Update();

            base.Update(gameTime);
        }

        private void UpdateInput(GameTime time)
        {

            if (newIS.controllers[0].Buttons.Back == ButtonState.Pressed || newIS.keyboard.IsKeyDown(Keys.Escape))
                this.Exit();

            if (!gameover)
            {
                if (oldIS.controllers[0].Buttons.A == ButtonState.Pressed & newIS.controllers[0].Buttons.A == ButtonState.Released)
                    launcher.projectiles.AddProjectile(100, 300, 20, 0);


                if (oldIS.controllers[0].Buttons.B == ButtonState.Pressed && newIS.controllers[0].Buttons.B == ButtonState.Released)
                {
                    launcher.SwitchAmmo();
                }

                if (newIS.controllers[0].DPad.Up == ButtonState.Pressed)
                    launcher.Move("up");

                if (newIS.controllers[0].DPad.Down == ButtonState.Pressed)
                    launcher.Move("down");

                if (newIS.controllers[0].Buttons.Y == ButtonState.Pressed & oldIS.controllers[0].Buttons.Y == ButtonState.Released)
                    chomper.SetMovingToNextTarget();
            }
            else
            {
                if (newIS.controllers[0].Buttons.X == ButtonState.Pressed & oldIS.controllers[0].Buttons.X == ButtonState.Released)
                    Restart(time);
            }
        }

        private void Restart(GameTime time)
        {

            gameover = false;

            launcher.hits = 0;
            launcher.misses = 0;
            launcher.health = 100;
            launcher.CurrentAmmoType = 0;

            chomper.wrong_foods_eaten = 0;
            chomper.desiredFood = 0;

            launcher.projectiles.Clear();

            remainingSeconds = 300;
            thisGameStartSeconds = (int) time.TotalGameTime.TotalSeconds;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SteelBlue);

            spriteBatch.Begin();

            if (!gameover)
            {
                Color healthColor = Color.Black;
                Color ammoColor = Color.Black;

                if (launcher.health == 0)
                    healthColor = Color.Red;

                int ammoPosition = 150;
                int hitPosition = 280;
                int missPosition = 350;
                int wrongPosition = 440;

                // DEBUG
                // spriteBatch.DrawString(quartz, "" + chomper.spriteArea + "\n" + chomper.GetNextTarget()+ "\n" + chomper.FramesNeeded + "\n" + chomper.pos_last + ", " +  chomper.pos_next, new Vector2(20 + 20, 4), healthColor);

                
                spriteBatch.DrawString(quartz_sm, "TIME", new Vector2(ammoPosition, 10), Color.Orange);
                spriteBatch.DrawString(quartz, ((int)remainingSeconds / 60) + ":" + ((((int)remainingSeconds % 60) > 9) ? ((int) remainingSeconds % 60).ToString() : "0" + ((int)remainingSeconds % 60)), new Vector2(ammoPosition + 54, 4), ammoColor);

                spriteBatch.DrawString(quartz_sm, "HITS", new Vector2(hitPosition, 10), Color.Orange);
                spriteBatch.DrawString(quartz, launcher.hits.ToString(), new Vector2(hitPosition + 40, 4), ammoColor);

                spriteBatch.DrawString(quartz_sm, "MISSES", new Vector2(missPosition, 10), Color.Orange);
                spriteBatch.DrawString(quartz, launcher.misses.ToString(), new Vector2(missPosition + 62, 4), ammoColor);

                spriteBatch.DrawString(quartz_sm, "WRONG", new Vector2(wrongPosition, 10), Color.Orange);
                spriteBatch.DrawString(quartz, chomper.wrong_foods_eaten.ToString(), new Vector2(wrongPosition + 64, 4), ammoColor);
                
                for (int c = 0; c < launcher.projectiles.max_size; c++)
                    if (launcher.projectiles.GetProjectile(c) != null)
                        if (launcher.projectiles.GetProjectile(c).alive)
                            launcher.projectiles.GetProjectile(c).Draw(gameTime);

                chomper.Draw(gameTime);

                launcher.currentAmmo.Draw(gameTime);

                launcher.Draw(gameTime);
            }
            else
            {
                String score = "";
                score += "Score: " + launcher.hits * 10 + "\n";
                score += "Hits: " + launcher.hits + "\n";
                score += "Misses: " + launcher.misses + "\n";
                score += "Wrong Foods: " + chomper.wrong_foods_eaten + "\n";

                spriteBatch.DrawString( quartz, score, new Vector2(10, 10), Color.Black);

                spriteBatch.DrawString(arial, "Credits:\nTed Senft - Programming\nTrevor Cicchinelli - MATH!", new Vector2(Window.ClientBounds.Width / 2 + 10, 10), Color.Black);

                spriteBatch.DrawString(quartz, "Press X to restart.", new Vector2(10, Window.ClientBounds.Height - quartz.MeasureString("Press X to restart.").Y), Color.Black);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
