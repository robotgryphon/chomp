using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

// Author: Ted Senft
// Completely re-written from scratch on July 30, 2013
// (Oh man, the all-nighter. It burns!)

// TODO: Implement a Frame.cs Class for sprite frame management,
// then add a way to manage sets of named sprite regions 
namespace Chomp_
{
    /// <summary>
    /// The sprite class. This class models a 2D sprite on-screen, whether it
    /// be animated or not. Still sprites will set isAnimated to false, and 
    /// will have a sheetSize of (1, 1). Animated sprites, give the necessary
    /// information needed.
    /// </summary>
    public class Sprite
    {

        public Game1 game;
        public Texture2D spritesheet;
        public Rectangle spriteArea;
        Vector2 spritesheetSize;
        Rectangle currentFrame;
        bool isAnimated;
        public SpriteEffects effect;
        public float rotation;
        public Vector2 origin;
        public int FrameDelay;
        private int timeSinceUpdate;
        public Boolean alive;
        public Boolean ground;
        public int alpha;

        public Sprite(Game1 game, string spriteFile, Point initPos, Vector2 sheetSize, bool animated)
        {
            this.game = game;
            this.spritesheet = game.Content.Load<Texture2D>(spriteFile);
            this.spriteArea = new Rectangle(initPos.X, initPos.Y, spritesheet.Bounds.Width, spritesheet.Bounds.Height);

            this.isAnimated = animated;

            this.spritesheetSize = sheetSize;

            this.currentFrame = new Rectangle(1, 1, spritesheet.Bounds.Width, spritesheet.Bounds.Height);

            this.origin = new Vector2(0, 0);

            this.FrameDelay = 30;
            this.timeSinceUpdate = 0;
            this.alive = true;
            this.ground = true;
            this.alpha = 255;
        }

        /// <summary>
        /// The main constructor. Creates a basic 2D sprite.
        /// </summary>
        /// <param name="game">A reference to the game object.</param>
        /// <param name="spriteFile">The path to the spritesheet image file.</param>
        /// <param name="initPos">The positioning the sprite should have onscreen.</param>
        /// <param name="spriteSize">The size, in pixels, of a -single- sprite in the sheet.</param>
        /// <param name="sheetSize">The dimensions, in full sprites, of the given spritesheet.</param>
        /// <param name="isAnimated">Whether or not the sprite is a single, static sprite or an animated series.</param>
        public Sprite(Game1 game, string spriteFile, Point initPos, Point spriteSize, Vector2 sheetSize, bool animated)
        {
            this.game = game;
            this.spritesheet = game.Content.Load<Texture2D>(spriteFile);
            this.spriteArea = new Rectangle(initPos.X, initPos.Y, spriteSize.X, spriteSize.Y);

            this.isAnimated = animated;

            // Make sure 1x1 sprites aren't animated. How could they be, anyway?
            if (spriteSize.X < 2 & spriteSize.Y < 2)
                this.isAnimated = false;

            this.spritesheetSize = sheetSize;

            this.currentFrame = new Rectangle(1, 1, spriteSize.X, spriteSize.Y);

            this.origin = new Vector2(0, 0);

            this.FrameDelay = 30;
            this.timeSinceUpdate = 0;
            this.alive = true;
            this.ground = true;
            this.alpha = 255;
        }

        /// <summary>
        /// Looping through the spritesheet, if applicable. Will be updated in the
        /// future to include named maps of sprite sets, to identify ranges.
        /// Example: "Attack1", "MovementHor", "Jump"
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public virtual void Update(GameTime gameTime)
        {
            // Update sprite stuff! Like spritesheet rendering.
            timeSinceUpdate += (int) gameTime.ElapsedGameTime.TotalMilliseconds;

            if (!game.paused)
            {
                if (timeSinceUpdate >= this.FrameDelay)
                {
                    

                    int frameX = currentFrame.X + spriteArea.Width * ((currentFrame.X / spriteArea.Width) + 1);
                    int frameY = currentFrame.Y;

                    if (frameX > (spritesheetSize.X * spriteArea.Width))
                    {
                        frameX = 1;
                        frameY += spriteArea.Height;
                        if (frameY > (spritesheetSize.Y * spriteArea.Height))
                            frameY = 1;
                    }

                    currentFrame = new Rectangle(
                        frameX,
                        frameY,
                        spriteArea.Width,
                        spriteArea.Height);

                    timeSinceUpdate = 0;
                }
            }
        }

        /// <summary>
        /// Draws the sprite on screen.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Draw(GameTime gameTime)
        {
            if(alive)
                game.spriteBatch.Draw(
                    spritesheet,
                    spriteArea,
                    currentFrame,
                    new Color(255, 255, 255, alpha),
                    rotation,
                    origin,
                    effect,
                    0);
        }
    }
}
